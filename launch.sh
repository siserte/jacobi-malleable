#!/bin/bash

##SBATCH --job-name=dmr
#SBATCH --output=slurm-dmr_%j.out
##SBATCH --error=slurm-dmr_%j.err

export PATH=$INSTALL_PATH/slurm/bin:$PATH
NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"
#echo $NODELIST
export DLB_ARGS="--talp"
export OMP_NUM_THREADS=1
time mpirun -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ./jacobi-malleable
