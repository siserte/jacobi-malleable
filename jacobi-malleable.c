#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <assert.h>
#include <sys/mman.h>
#include "omp.h"
#include <time.h>

#define ITERATIONS 10
// #define SIZE 32768
// #define SIZE 16384
// #define SIZE 8192
#define SIZE 256
#define SIZE 512

#include "dmr.h"

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

void Jacobi(double *aFlat, double *x, double *b, int size, int jobid)
{
    int rank_size, myrank;
    MPI_Comm_size(DMR_INTERCOMM, &rank_size);
    MPI_Comm_rank(DMR_INTERCOMM, &myrank);
    int i, j, sum1, sum2, i_global;
    int *count = (int *)malloc(sizeof(int) * rank_size);
    int *displacements = (int *)malloc(sizeof(int) * rank_size);

    /* copy current solution vector so we can update it */
    double *xold = (double *)malloc(size * sizeof(double));
    memcpy(xold, x, size * sizeof(double));

    for (i = 0; i < rank_size; i++)
    {
        count[i] = (int)floor(size / rank_size);
        displacements[i] = i * count[i];
    }
    count[rank_size - 1] = size - ((int)floor(size / rank_size)) * (rank_size - 1);
    // rows local
    int rows_local = (int)floor(size / rank_size);
    int local_offset = myrank * rows_local;
    if (myrank == (rank_size - 1))
        rows_local = size - rows_local * (rank_size - 1);

    int len;
    char name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(name, &len);
    //printf("*Log*\tPID:%d\tTimestep:%d\tJob:%d\tHost:%s\tSize:%d\tIteration:%d\n", getpid(), (int)time(NULL), jobid, name, rank_size, DMR_it);

#pragma omp parallel
    {
        if (myrank == 0 && omp_get_thread_num() == 0)
        {
            printf("(Job %d PID %d in %s)[%d/%d]: %s(%s,%d) PROCESSING STEP %d with %d OpenMP threads\n", jobid, getpid(), name, myrank, rank_size, __FILE__, __func__, __LINE__, DMR_it, omp_get_num_threads());
        }
#pragma omp for private(j, sum1, sum2)
        for (i = 0; i < rows_local; i++)
        {
            i_global = local_offset + i;
            sum1 = 0.0;
            sum2 = 0.0;
            for (j = 0; j < i_global; j++)
            {
                sum1 = sum1 + aFlat[size * i + j] * xold[j];
            }
            for (j = i_global + 1; j < size; j++)
                sum2 = sum2 + aFlat[size * i + j] * xold[j];
            x[i] = (-sum1 - sum2 + b[i]) / aFlat[size * i + i_global];
        }
    }
    MPI_Allgatherv(x, rows_local, MPI_DOUBLE, xold, count, displacements, MPI_DOUBLE, DMR_INTERCOMM);
    sleep(1);
}

void initialize_data(double **b, double **x, double **aFlat, int n)
{
    double t0 = MPI_Wtime();
    (*b) = (double *)malloc(sizeof(double) * n);
    (*x) = (double *)malloc(sizeof(double) * n);
    (*aFlat) = (double *)malloc(sizeof(double) * n * n);
    for (int i = 0; i < n; i++)
    {
        (*b)[i] = fRand(0, 1000);
        for (int j = 0; j < n; j++)
        {
            (*aFlat)[n * i + j] = fRand(0, 1000);
        }
        (*x)[i] = 1.0;
    }
    double t1 = MPI_Wtime();
    fflush(NULL);
    if (DMR_comm_rank == 0)
        printf("Matrix of %d elements generated in %g seconds.\n", n, t1 - t0);
}

void free_data(double **b, double **x, double **aFlat)
{
    free((*x));
    free((*b));
    free((*aFlat));
}

void gather(MPI_Comm comm, double *send_data, double **recv_data, int n)
{
    int send_size, comm_size, rank;
    // printf("(sergio)[%d/%d] %d: %s(%s,%d)\n", DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__);
    MPI_Comm_size(comm, &comm_size);
    MPI_Comm_rank(comm, &rank);
    send_size = n / comm_size;
    if (rank == 0)
    {
        (*recv_data) = (double *)malloc(n * sizeof(double));
    }
    MPI_Gather(send_data, send_size, MPI_DOUBLE, (*recv_data), send_size, MPI_DOUBLE, 0, comm);
    MPI_Barrier(comm);
}

void scatter(MPI_Comm comm, double **send_data, double **recv_data, int n)
{
    int recv_size, comm_size, rank;
    // printf("(sergio)[%d/%d] %d: %s(%s,%d)\n", DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__);
    MPI_Comm_size(comm, &comm_size);
    MPI_Comm_rank(comm, &rank);
    recv_size = n / comm_size;
    (*recv_data) = (double *)malloc(recv_size * sizeof(double));
    MPI_Scatter((*send_data), recv_size, MPI_DOUBLE, (*recv_data), recv_size, MPI_DOUBLE, 0, comm);
    if (DMR_comm_rank == 0)
    {
        free(*send_data);
    }
    // printf("(sergio)[%d/%d] %d: %s(%s,%d)\n", DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__);
    // MPI_Barrier(DMR_INTERCOMM);
}

/* Executed only by existent ranks */
void send_expand(double **vector, int n)
{
    double *gathered_data = NULL;
    /* Step 1: Gather on old_comm (to rank 0) */
    gather(DMR_COMM_OLD, *vector, &gathered_data, n);

    /* Step 2: Scatter on new comm (from rank 0) */
    scatter(DMR_COMM_NEW, &gathered_data, vector, n);
}

/* Executed only by newly added ranks */
void recv_expand(double **vector, int n)
{
    double *dummy_ptr = NULL;
    /* Step 1: Scatter on new comm (from rank 0) */
    scatter(DMR_COMM_NEW, &dummy_ptr, vector, n);
}

/* Executed only by leaving processes */
void send_shrink(double **vector, int n)
{
    double *dummy_ptr = NULL;
    /* Step 1: Gather on old_comm (to rank 0) */
    gather(DMR_COMM_OLD, *vector, &dummy_ptr, n);
}

/* Executed only by staying processes */
void recv_shrink(double **vector, int n)
{

    double *gathered_data = NULL;
    /* Step 1: Gather on old_comm (to rank 0) */
    gather(DMR_COMM_OLD, *vector, &gathered_data, n);

    /* Step 2: Scatter on new comm (from rank 0) */
    scatter(DMR_COMM_NEW, &gathered_data, vector, n);
}

/* Executed only by existent ranks */
void send_expand_all(double **b, double **x, double **aFlat, int n)
{
    send_expand(b, n);
    send_expand(x, n);
    send_expand(aFlat, n * n);
}

/* Executed only by newly added ranks */
void recv_expand_all(double **b, double **x, double **aFlat, int n)
{
    recv_expand(b, n);
    recv_expand(x, n);
    recv_expand(aFlat, n * n);
}

/* Executed only by leaving processes */
void send_shrink_all(double **b, double **x, double **aFlat, int n)
{
    send_shrink(b, n);
    send_shrink(x, n);
    send_shrink(aFlat, n * n);
}

/* Executed only by staying processes */
void recv_shrink_all(double **b, double **x, double **aFlat, int n)
{
    recv_shrink(b, n);
    recv_shrink(x, n);
    recv_shrink(aFlat, n * n);
}

// USAGE ./jacobi_malleable <jobid>
int main(int argc, char **argv)
{
    srand(1);
    double *x, *b;
    double *aFlat;
    int jobid = atoi(argv[1]);

    // printf("(sergio) Let's start!\n");
    DMR_INIT(ITERATIONS, initialize_data(&b, &x, &aFlat, SIZE), recv_expand_all(&b, &x, &aFlat, SIZE));
    // printf("(sergio)[%d/%d] %d: %s(%s,%d) PROCESSING STEP %d\n", DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__, DMR_it);
    DMR_Set_parameters(8, 64, 32);
    for (; DMR_it < ITERATIONS + 1; DMR_it++)
    {
        // printf("(Job %d)[%d/%d] %d: %s(%s,%d) PREV %d CURR %d\n", jobid, DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__, DMR_comm_prev_size, DMR_comm_size);
        Jacobi(aFlat, x, b, SIZE, jobid);
        DMR_RECONFIGURATION(send_expand_all(&b, &x, &aFlat, SIZE), recv_expand_all(&b, &x, &aFlat, SIZE), send_shrink_all(&b, &x, &aFlat, SIZE), recv_shrink_all(&b, &x, &aFlat, SIZE));
        // printf("[%d/%d] %d: %s(%s,%d): It %d/%d\n", DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__, DMR_it, ITERATIONS);
    }
    // printf("[%d/%d] %d: %s(%s,%d): OUT It %d/%d\n", DMR_comm_rank, DMR_comm_size, getpid(), __FILE__, __func__, __LINE__, DMR_it, ITERATIONS);

    if (DMR_comm_rank == 0)
    {
        FILE *fp;
        /* read the values */
        if ((fp = fopen("test.sergio", "w")) == NULL)
        {
            printf("Cannot open file.\n");
        }
        for (int i = 0; i < SIZE; i++)
        {
            fprintf(fp, "%f ", x[i]);
        }
        fprintf(fp, "\n");
        fclose(fp);
    }

    DMR_FINALIZE(free_data(&b, &x, &aFlat));
    return 0;
}


/*
void sendEx(double *aFlat, double *x, double *b, int size)
{
    int myrank, intercomm_size, comm_size, i, factor, dst;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);
    factor = intercomm_size / comm_size;

    for (i = 0; i < factor; i++)
    {
        dst = myrank * factor + i;
        MPI_Send(aFlat, size * size, MPI_DOUBLE, dst, 0, DMR_INTERCOMM);
        MPI_Send(x, size, MPI_DOUBLE, dst, 1, DMR_INTERCOMM);
        MPI_Send(b, size, MPI_DOUBLE, dst, 2, DMR_INTERCOMM);
    }
}

void recvEx(double **aFlat, double **x, double **b, int *size)
{
    int myrank, parent_size, comm_size, factor, src;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Status status;
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    factor = comm_size / parent_size;

    *aFlat = (double *)malloc((*size) * (*size) * sizeof(double));
    *x = (double *)malloc((*size) * sizeof(double));
    *b = (double *)malloc((*size) * sizeof(double));
    src = myrank / factor;

    MPI_Recv(*aFlat, (*size) * (*size), MPI_DOUBLE, src, 0, DMR_INTERCOMM, &status);
    MPI_Recv(*x, (*size), MPI_DOUBLE, src, 1, DMR_INTERCOMM, &status);
    MPI_Recv(*b, (*size), MPI_DOUBLE, src, 2, DMR_INTERCOMM, &status);
}

void sendSh(double *aFlat, double *x, double *b, int size)
{
    int myrank, intercomm_size, comm_size, dst;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);

    // envían sólo los ranks que tienen un homónimo en intercomm (envío de todo el struct).
    dst = myrank;
    if (dst < intercomm_size)
    {
        MPI_Send(aFlat, size * size, MPI_DOUBLE, dst, 0, DMR_INTERCOMM);
        MPI_Send(x, size, MPI_DOUBLE, dst, 1, DMR_INTERCOMM);
        MPI_Send(b, size, MPI_DOUBLE, dst, 2, DMR_INTERCOMM);
    }
}

void recvSh(double **aFlat, double **x, double **b, int *size)
{
    int myrank, parent_size, comm_size, src;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Status status;
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);

    *aFlat = (double *)malloc((*size) * (*size) * sizeof(double));
    *x = (double *)malloc((*size) * sizeof(double));
    *b = (double *)malloc((*size) * sizeof(double));

    src = myrank;
    MPI_Recv(*aFlat, (*size) * (*size), MPI_DOUBLE, src, 0, DMR_INTERCOMM, &status);
    MPI_Recv(*x, (*size), MPI_DOUBLE, src, 1, DMR_INTERCOMM, &status);
    MPI_Recv(*b, (*size), MPI_DOUBLE, src, 2, DMR_INTERCOMM, &status);
}
*/