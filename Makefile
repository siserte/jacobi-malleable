#DIR     =       /opt/hpc/build/
#MPIDIR 	=	/opt/hpc/install/ompi
#DMRDIR  =	$(DIR)dmr
#DMRFLAGS        = -I$(DMRDIR) -L$(DMRDIR) -ldmr
FLAGS           = -O3 -Wall -Wuninitialized -Wmaybe-uninitialized -g
#MPIFLAGS    	= -I$(MPIDIR)/include -L$(MPIDIR)/lib -lmpi

#all: clean jacobi-malleable 
all: clean jacobi-original

jacobi-original: jacobi-original.c
	mpic++ $(FLAGS) -fopenmp jacobi-original.c -o jacobi-original

jacobi-malleable: jacobi-malleable.c
	mpic++ $(FLAGS) $(MPIFLAGS) $(DMRFLAGS) -fopenmp jacobi-malleable.c -o jacobi-malleable
	
clean:
	rm -f *.out *.o kernels/*.o data/*.out *.dot *.pdf *.intel64 *.mic *.INTEL64 jacobi-malleable *.dmr jacobi-original
