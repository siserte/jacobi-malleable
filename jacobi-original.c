#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <assert.h>
#include <sys/mman.h>
#include "omp.h"
#include <time.h>
#include "mpi.h"

#define ITERATIONS 100
//#define SIZE 32768
//#define SIZE 16384
#define SIZE 8192
//#define SIZE 256
//#define SIZE 512

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

void Jacobi(double *aFlat, double *x, double *b, int size, int it)
{
    int rank_size, myrank;
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    int i, j, sum1, sum2, i_global;
    int *count = (int *)malloc(sizeof(int) * rank_size);
    int *displacements = (int *)malloc(sizeof(int) * rank_size);

    /* copy current solution vector so we can update it */
    double *xold = (double *)malloc(size * sizeof(double));
    memcpy(xold, x, size * sizeof(double));

    for (i = 0; i < rank_size; i++)
    {
        count[i] = (int)floor(size / rank_size);
        displacements[i] = i * count[i];
    }
    count[rank_size - 1] = size - ((int)floor(size / rank_size)) * (rank_size - 1);
    // rows local
    int rows_local = (int)floor(size / rank_size);
    int local_offset = myrank * rows_local;
    if (myrank == (rank_size - 1))
        rows_local = size - rows_local * (rank_size - 1);

    int len;
    char name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(name, &len);

#pragma omp parallel
    {
       if (myrank == 0 && omp_get_thread_num() == 0)
               printf("(PID %d in %s)[%d/%d]: %s(%s,%d) PROCESSING STEP %d/%d with %d OpenMP threads\n", getpid(), name, myrank, rank_size, __FILE__, __func__, __LINE__, it, ITERATIONS, omp_get_num_threads());
        
#pragma omp for private(j, sum1, sum2)
        for (i = 0; i < rows_local; i++)
        {
            i_global = local_offset + i;
            sum1 = 0.0;
            sum2 = 0.0;
            for (j = 0; j < i_global; j++)
            {
                sum1 = sum1 + aFlat[size * i + j] * xold[j];
            }
            for (j = i_global + 1; j < size; j++)
                sum2 = sum2 + aFlat[size * i + j] * xold[j];
            x[i] = (-sum1 - sum2 + b[i]) / aFlat[size * i + i_global];
        }
    }
    MPI_Allgatherv(x, rows_local, MPI_DOUBLE, xold, count, displacements, MPI_DOUBLE, MPI_COMM_WORLD);
    sleep(1);
}

void initialize_data(double **b, double **x, double **aFlat, int n)
{
    double t0 = MPI_Wtime();
    (*b) = (double *)malloc(sizeof(double) * n);
    (*x) = (double *)malloc(sizeof(double) * n);
    (*aFlat) = (double *)malloc(sizeof(double) * n * n);
    for (int i = 0; i < n; i++)
    {
        (*b)[i] = fRand(0, 1000);
        for (int j = 0; j < n; j++)
        {
            (*aFlat)[n * i + j] = fRand(0, 1000);
        }
        (*x)[i] = 1.0;
    }
    double t1 = MPI_Wtime();
    fflush(NULL);
}

void free_data(double **b, double **x, double **aFlat)
{
    free((*x));
    free((*b));
    free((*aFlat));
}

// USAGE ./jacobi_malleable <jobid>
int main(int argc, char **argv)
{
    srand(1);
    double *x, *b;
    double *aFlat;

    MPI_Init(&argc, &argv);

    initialize_data(&b, &x, &aFlat, SIZE);
    for (int i = 0; i < ITERATIONS; i++)
    {
        Jacobi(aFlat, x, b, SIZE, i);
    }

    free_data(&b, &x, &aFlat);

    MPI_Finalize();
    return 0;
}

